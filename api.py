from modules.clean_data import clean_uv
from sklearn.experimental import enable_iterative_imputer
from sklearn.impute import IterativeImputer
from statsmodels.tsa.arima.model import ARIMA
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

class learn_uv:
    def __init__(self, date0: str, date1: str, add_na: bool, gap_time: int) -> None:
        self.date0 = date0
        self.date1 = date1
        self.add_na = add_na
        self.gap_time = gap_time
        self.pandas_data = clean_uv(self.date0, self.date1, self.add_na, self.gap_time)
        self.numpy_data = self.pandas_data.to_numpy()


    def impute_missing(self, method: str) -> np.ndarray:
        if method == 'mean_group_by_time':
            data = self.pandas_data
            data['hour'] =  pd.to_datetime(self.pandas_data.timestamp)
            data.hour = pd.to_datetime(data.hour.dt.strftime('%H:%M:%S'))
            data['uv'] = data['uv'].fillna(data.groupby(data.hour.dt.floor('10T'))['uv'].transform('mean'))
            data['uv_a'] = data['uv_a'].fillna(data.groupby(data.hour.dt.floor('10T'))['uv_a'].transform('mean'))
            data['uv_b'] = data['uv_b'].fillna(data.groupby(data.hour.dt.floor('10T'))['uv_b'].transform('mean'))
            data = data.drop('hour', axis=1)
            data = np.array(data, dtype='float64')
        elif method == 'iterative_imputer':
            data = self.numpy_data
            imputer = IterativeImputer(max_iter = 100)
            imputer.fit(X=data, y=None)
            data = imputer.transform(X=data)
        return data



if __name__ == "__main__":
    uv = learn_uv(date0='2023-05-01', date1='2023-05-16', add_na=True, gap_time=300 * 10**9)

    start_date = pd.to_datetime(uv.numpy_data[-1, 3])
    end_date = start_date + pd.DateOffset(months=1)
    prediction_data = pd.date_range(start=start_date, end=end_date, freq='3min')

    data0 = np.array(uv.numpy_data, dtype='float64')
    model0 = ARIMA(endog=data0[:,0], order=(1,1,1))
    model_fit0 = model0.fit()
    predictions0 = model_fit0.predict(start=0, end=len(prediction_data)-1)

    plt.subplot(2,1,1)
    plt.plot(pd.to_datetime(uv.numpy_data[:,3]), uv.numpy_data[:,0], color='blue', label='Raw data')
    plt.plot(prediction_data, predictions0, color='red', label='Predicted data')
    plt.ylabel('UV')
    plt.legend()
    plt.show()