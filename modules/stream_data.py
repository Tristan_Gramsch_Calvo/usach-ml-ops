import urllib.request
import re
import os.path
import time
import psycopg2
import pandas as pd


def process_data(file):
    # Format the file
    file_name = file
    try:
        df = pd.read_csv(file_name, sep='\t', header=None)
    except pd.errors.EmptyDataError:
        print('There is no data in this file. Returning')
        return
    df = df.tail(-1) # Remove first row
    df = df.iloc[:, 0].str.split(expand=True) # Produce one column per element splitted. Elements are being splitted by white spaces.
    df.columns = ['Hora', 'Indice_UV', 'UV-B_(uW/cm2)', 'UV-A_(mW/cm2)']
    df = df.replace(',', '.', regex=True)
    date = file_name.split('.')[0].split('-') # Get the date from the file name
    date = date[2] + '-' + date[1] + '-' + date[0].split('UV')[1]
    df['Fecha'] = date
    
    # Save to database.
    conn = psycopg2.connect(dbname='usach-db', user='tristan', password='tristan', host='localhost')
    cur = conn.cursor()
    for index, row in df.iterrows():
        try:
            cur.execute("INSERT INTO uv_datos_horarios (fecha, hora, uv, uv_b, uv_a) VALUES (%s, %s, %s, %s, %s)", (row['Fecha'], row['Hora'], row['Indice_UV'], row['UV-B_(uW/cm2)'], row['UV-A_(mW/cm2)']))
            print('Data added: ', row['Fecha'], row['Hora'], row['Indice_UV'], row['UV-B_(uW/cm2)'], row['UV-A_(mW/cm2)'])
        except:
            # print('There is probably already a record for this date and hour. Going for the next value.')
            pass
    conn.commit()
    cur.close()
    conn.close()


if __name__ == '__main__':    
    while True:
        url = 'http://ambiente.usach.cl/uv/Datos_horarios/'
        response = urllib.request.urlopen(url)
        html = response.read()
        html = html.decode('utf-8')
        lines = html.split('\n')
        if not os.path.exists('data'):
            os.makedirs('data')
        for line in lines:
            regex = r'href="(.+?-2023.dat)"'
            match = re.search(regex, line)
            if match:
                name = match.group(1)
                if not os.path.isfile('data/' + name):
                    try:
                        urllib.request.urlretrieve(url + name, 'data/' + name)
                        print("File downloaded")
                    except:
                        os.remove('data/' + name)
                        print("Error downloading file, deleting.")
                else:
                    print("File already in local machine")

                if name == f"""UV{time.strftime("%d-%m-%Y")}.dat""":
                    print("Checking for updates in today's file")    
                    try:
                        urllib.request.urlretrieve(url + name, 'data/' + name)
                        print("File updated")
                    except:
                        os.remove('data/' + name)
                process_data('data/' + name)
        print("Wait 10 minutes")
        time.sleep(600)