import psycopg2
import pandas as pd
import numpy as np


def clean_uv(date_1: str, date_2: str, add_na: bool = True, gap_time: int = 300*10**9) -> np.ndarray:
    def decimal_to_hms(decimal_hour):
        hour = int(decimal_hour)
        min = int((decimal_hour - hour) * 60)
        sec = int(((decimal_hour - hour) * 60 - min) * 60)
        return f"{hour}:{min}:{sec}"
    
    conn = psycopg2.connect(dbname='usach-db', user='tristan', password='tristan', host='localhost')
    cur = conn.cursor()
    cur.execute("SELECT fecha, hora, uv, uv_b, uv_a FROM uv_datos_horarios where fecha between '{}' and '{}'".format(date_1, date_2))
    data = cur.fetchall()
    
    data = pd.DataFrame(data, columns=['fecha', 'hora', 'uv', 'uv_b', 'uv_a'])
    data.hora = data.hora.apply(lambda x: decimal_to_hms(x))
    data.fecha = pd.to_datetime(data.fecha.astype(str) + ' ' + data.hora.astype(str))
    data['timestamp'] = data.fecha.astype(np.int64)
    data = data.drop(['fecha', 'hora'], axis=1)
    
    # Add missing timestamps data in gaps.
    if add_na:
        new_rows = []
        for i in range(len(data)-1):
            diff = data.iloc[i+1].timestamp - data.iloc[i].timestamp
            if diff > gap_time:
                num_rows = int(diff/gap_time)
                for j in range(num_rows):
                    new_timestamp = data.iloc[i].timestamp + (j+1)*gap_time
                    new_row = [np.nan, np.nan, np.nan, new_timestamp]
                    new_rows.append(new_row)
        
        new_data = pd.DataFrame(new_rows, columns=data.columns)
        data = data.append(new_data, ignore_index=True)
    return data


if __name__ == "__main__":
    data = clean_uv('2014-01-01', '2014-01-05', add_na=True, gap_time=300 * 10**9)

