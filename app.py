from flask import Flask
import numpy as np
import pandas as pd
from api import learn_uv
from statsmodels.tsa.arima.model import ARIMA

uv = learn_uv(date0='2023-05-01', date1='2023-05-16', add_na=True, gap_time=300 * 10**9)

start_date = pd.to_datetime(uv.numpy_data[-1, 3])
end_date = start_date + pd.DateOffset(months=1)
prediction_data = pd.date_range(start=start_date, end=end_date, freq='3min')

data0 = np.array(uv.numpy_data, dtype='float64')
model0 = ARIMA(endog=data0[:,0], order=(1,1,1))
model_fit0 = model0.fit()
predictions0 = model_fit0.predict(start=0, end=len(prediction_data)-1)
predictions0 = predictions0.tolist()

prediction_data = prediction_data.tolist()

prediction_df = pd.DataFrame(data={'prediction': predictions0, 'date': prediction_data})

app = Flask(__name__)

@app.route("/")
def hello_world():
    return prediction_df.to_html()